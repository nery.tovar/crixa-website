$(document).ready(function() {
  // Efecto hover
  $("#img-mkt").hover(function() {
    $("#img-mkt").attr("src", "img/marketing-digital-sombreado.jpg");
  }, function() {
    $("#img-mkt").attr("src", "img/marketing-digital.jpg");
  });

  $("#img-pbl").hover(function() {
    $("#img-pbl").attr("src", "img/publicidad-exterior-sombreado.jpg");
  }, function() {
    $("#img-pbl").attr("src", "img/publicidad-exterior.jpg");
  });

  $("#img-btl").hover(function() {
    $("#img-btl").attr("src", "img/btl-sombreado.jpg");
  }, function() {
    $("#img-btl").attr("src", "img/btl.jpg");
  });

  $("#img-serv").hover(function() {
    $("#img-serv").attr("src", "img/servicios-adicionales-sombreado.jpg");
  }, function() {
    $("#img-serv").attr("src", "img/servicios-adicionales.jpg");
  });

  // Evento click
  $("#img-mkt").click(function() {
    $("#modal-mkt").modal("show");
  });

  $("#img-pbl").click(function() {
    $("#modal-pbl").modal("show");
  });

  $("#img-btl").click(function() {
    $("#modal-btl").modal("show");
  });

  $("#img-serv").click(function() {
    $("#modal-serv").modal("show");
  });
});